---
title: "TP3"
author: "Ariel Lissak Geller / Victorien Lecomte / Anna Nerard"
date: '2022-03-11'
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

#Question 1

```{r}
#variables
mu<-1
sigma<-2
N<-1000

#echantillon pour n=5
S5<-array(c(0),dim=c(N,5))
for(i in 1:N){
  S5[i,1:5]<-rnorm(5,mu,sigma^2)
}
#echantillon pour n=30
S30<-array(c(0),dim=c(N,30))
for(i in 1:N){
  S30[i,1:30]<-rnorm(30,mu,sigma^2)
}
#echantillon pour n=100
S100<-array(c(0),dim=c(N,100))
for(i in 1:N){
  S100[i,1:100]<-rnorm(100,mu,sigma^2)
}

#recuperation des moyennes
moyennes_empirique5<-array(0,dim=c(1,N))
for(i in 1:N){
  moyennes_empirique5[1,i]<-mean(S5[i,1:5])
}   

moyennes_empirique30<-array(0,dim=c(1,N))
for(i in 1:N){
  moyennes_empirique30[1,i]<-mean(S30[i,1:30])
}   

moyennes_empirique100<-array(0,dim=c(1,N))
for(i in 1:N){
  moyennes_empirique100[1,i]<-mean(S100[i,1:100])
}   

#recuperation des variances
variance_empirique5<-array(0,dim=c(1,N))
for(i in 1:N){
  variance_empirique5[1,i]<-sd(S5[i,1:5])
}   

variance_empirique30<-array(0,dim=c(1,N))
for(i in 1:N){
  variance_empirique30[1,i]<-sd(S30[i,1:30])
}   

variance_empirique100<-array(0,dim=c(1,N))
for(i in 1:N){
  variance_empirique100[1,i]<-sd(S100[i,1:100])
}   

par(mfrow=c(1,3))

hist(moyennes_empirique5)
hist(moyennes_empirique30)
hist(moyennes_empirique100)

```



#Question 2

```{r}
library("Pareto")
#variables
a<-1
alpha<-2
N<-1000
#echantillon pour n=5
T5<-array(c(0),dim=c(N,5))
for(i in 1:N){
  T5[i,1:5]<-rPareto(5,a,alpha)
}
#echantillon pour n=30
T30<-array(c(0),dim=c(N,30))
for(i in 1:N){
  T30[i,1:30]<-rPareto(30,a,alpha)
}
#echantillon pour n=100
T100<-array(c(0),dim=c(N,100))
for(i in 1:N){
  T100[i,1:100]<-rPareto(100,a,alpha)
}

#recuperation des moyennes
moyennes_empirique_bis5<-array(0,dim=c(1,N))
for(i in 1:N){
  moyennes_empirique_bis5[1,i]<-mean(T5[i,1:5])
}   

moyennes_empirique_bis30<-array(0,dim=c(1,N))
for(i in 1:N){
  moyennes_empirique_bis30[1,i]<-mean(T30[i,1:30])
}   

moyennes_empirique_bis100<-array(0,dim=c(1,N))
for(i in 1:N){
  moyennes_empirique_bis100[1,i]<-mean(T100[i,1:100])
}   

#recuperation des variances
variance_empirique_bis5<-array(0,dim=c(1,N))
for(i in 1:N){
  variance_empirique_bis5[1,i]<-sd(T5[i,1:5])
}   

variance_empirique_bis30<-array(0,dim=c(1,N))
for(i in 1:N){
  variance_empirique_bis30[1,i]<-sd(T30[i,1:30])
}   

variance_empirique_bis100<-array(0,dim=c(1,N))
for(i in 1:N){
  variance_empirique_bis100[1,i]<-sd(T100[i,1:100])
}   


```


#Question 3


#Question 4


#Question 5

Une facon simple d'estimer p est de faire une simulation avec un nombre 
d'échantillon élévé puis de faire une moyenne empirique du résultat.
```{r}
#on génère 100 variables de Bernoulli de proba 0.7
x<-rbinom(100,1,0.7)

#on calcule la moyenne empirique
mean(x)

#on a la valeur 0.64
```

#Question 6

Pour une loi de Bernoulli on nomme p la probabilité à tester
Avec x l'échantillon 
```{r}
#Ainsi on définit la fonction L_Bern
L_Bern <- function(p,x) {
  s<-0
  for (k in 1:length(x))
    s<-s+x[k]
  vraissemblance <-(p^s)*((1-p)^(length(x)-s))
  vraissemblance
}
L_Bern(0.7,100)
```

#Question 7

Soit x le vecteur a tester
```{r}

L_Bern <- function(p,x) {
  s<-0
  for (k in 1:length(x))
    s<-s+x[k]
  vraissemblance <-(p^s)*((1-p)^(length(x)-s))
  vraissemblance
}

#generation de l'absice
Generation_y <- function (x){
  y<-vector()
  for (k in c(0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1)){
    y<-c(y,L_Bern(k,x))}
    y
}

x<-rbinom(100,1,0.7)

y<- Generation_y(x)

#affichage
plot(c(0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1),y)

#Avec le code suivant :
optimize(L_Bern, c(0,1), tol=0.001, x= x, maximum = TRUE)
#on obtient le maximum de la fonction. Ici notre 
#échantillon initial nous permet de trouver $maximum = 0.73981 
#Ce qui est proche de notre probabilité (0.7)
```

#Question 8

```{r}
Generation_echantillons <- function(){
  maximum<-vector()
  for (k in c(10,25,50,100,200,500,100,2000)){
    x<-rbinom(k,1,0.6)
    maximum <- c(maximum,optimize(L_Bern, c(0,1), tol=0.001, x= x, maximum = TRUE)[1])
  }
  maximum
}
```
On observe que plus la taille de l'échantillon augmente, plus la probabilité calculée est proche de celle qui a permit de generer l'échantillon.

#Question 9

#Question 10

```{r}
#Variables
mu<-2
sigma<-1
#Ainsi on définit la fonction L_norm
L_norm <- function(mu,sigma,x) {
  vraissemblance<-0
  dens<-rnorm(x,mu,sigma)
  for (k in 1:length(x))
    vraissemblance <- vraissemblance+dnorm(dens[k],mu, sigma)
  vraissemblance
}
L_norm(2,1,100)

```
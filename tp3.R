#Question 1
#variables
mu<-1
sigma<-2
N<-1000
#echantillon pour n=5
S5<-array(c(0),dim=c(N,5))
for(i in 1:N){
  S5[i,1:5]<-rnorm(5,mu,sigma^2)
}
#echantillon pour n=30
S30<-array(c(0),dim=c(N,30))
for(i in 1:N){
  S30[i,1:30]<-rnorm(30,mu,sigma^2)
}
#echantillon pour n=100
S100<-array(c(0),dim=c(N,100))
for(i in 1:N){
  S100[i,1:100]<-rnorm(100,mu,sigma^2)
}

#recuperation des moyennes
moyennes_empirique5<-array(0,dim=c(1,N))
for(i in 1:N){
  moyennes_empirique5[1,i]<-mean(S5[i,1:5])
}   

moyennes_empirique30<-array(0,dim=c(1,N))
for(i in 1:N){
  moyennes_empirique30[1,i]<-mean(S30[i,1:30])
}   

moyennes_empirique100<-array(0,dim=c(1,N))
for(i in 1:N){
  moyennes_empirique100[1,i]<-mean(S100[i,1:100])
}   

#recuperation des variances
variance_empirique5<-array(0,dim=c(1,N))
for(i in 1:N){
  variance_empirique5[1,i]<-sd(S5[i,1:5])
}   

variance_empirique30<-array(0,dim=c(1,N))
for(i in 1:N){
  variance_empirique30[1,i]<-sd(S30[i,1:30])
}   

variance_empirique100<-array(0,dim=c(1,N))
for(i in 1:N){
  variance_empirique100[1,i]<-sd(S100[i,1:100])
}   

par(mfrow=c(1,3))

hist(moyennes_empirique5)
hist(moyennes_empirique30)
hist(moyennes_empirique100)

# la moyenne empirique tend vers l'esperance pour la loi theorique
## à continuer

#Question 2
library("Pareto")
#variables
a<-1
alpha<-2
N<-1000
#echantillon pour n=5
T5<-array(c(0),dim=c(N,5))
for(i in 1:N){
  T5[i,1:5]<-rPareto(5,a,alpha)
}
#echantillon pour n=30
T30<-array(c(0),dim=c(N,30))
for(i in 1:N){
  T30[i,1:30]<-rPareto(30,a,alpha)
}
#echantillon pour n=100
T100<-array(c(0),dim=c(N,100))
for(i in 1:N){
  T100[i,1:100]<-rPareto(100,a,alpha)
}

#recuperation des moyennes
moyennes_empirique_bis5<-array(0,dim=c(1,N))
for(i in 1:N){
  moyennes_empirique_bis5[1,i]<-mean(T5[i,1:5])
}   

moyennes_empirique_bis30<-array(0,dim=c(1,N))
for(i in 1:N){
  moyennes_empirique_bis30[1,i]<-mean(T30[i,1:30])
}   

moyennes_empirique_bis100<-array(0,dim=c(1,N))
for(i in 1:N){
  moyennes_empirique_bis100[1,i]<-mean(T100[i,1:100])
}   

#recuperation des variances
variance_empirique_bis5<-array(0,dim=c(1,N))
for(i in 1:N){
  variance_empirique_bis5[1,i]<-sd(T5[i,1:5])
}   

variance_empirique_bis30<-array(0,dim=c(1,N))
for(i in 1:N){
  variance_empirique_bis30[1,i]<-sd(T30[i,1:30])
}   

variance_empirique_bis100<-array(0,dim=c(1,N))
for(i in 1:N){
  variance_empirique_bis100[1,i]<-sd(T100[i,1:100])
}   



# Question 10



